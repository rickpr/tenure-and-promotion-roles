class CreatePolicies < ActiveRecord::Migration[5.1]
  def change
    create_table :policies do |t|
      t.references :subject, foreign_key: true
      t.references :object, foreign_key: true
      t.string :action

      t.timestamps
    end
  end
end
