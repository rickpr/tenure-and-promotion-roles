class CreateMasterRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :master_roles do |t|
      t.string :name
      t.string :context

      t.timestamps
    end
  end
end
