class Role < ApplicationRecord
  has_and_belongs_to_many :users, join_table: :users_roles

  belongs_to :resource, polymorphic: true, optional: true

  validates :resource_type, inclusion: { in: Rolify.resource_types }, allow_nil: true

  scopify

  def check_resource
    unless MasterRole.all.map(&:name).include?(name)
      errors.add("This is not a valid role name")
      return
    end
    unless MasterRole.find_by(name: name).resource_type == resource_type
      errors.add("This role cannot be applied to a #{resource_type}")
    end
  end
end
