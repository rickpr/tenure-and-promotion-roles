class College < ApplicationRecord
  resourcify
  has_many :departments
end
