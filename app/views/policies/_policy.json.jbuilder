json.extract! policy, :id, :subject_id, :object_id, :action, :created_at, :updated_at
json.url policy_url(policy, format: :json)
