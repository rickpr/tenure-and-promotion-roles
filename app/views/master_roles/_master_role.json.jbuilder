json.extract! master_role, :id, :name, :context, :created_at, :updated_at
json.url master_role_url(master_role, format: :json)
