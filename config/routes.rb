Rails.application.routes.draw do
  resources :policies
  resources :master_roles
  resources :colleges
  resources :departments
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
