|Role|Context|
|:--:|:--:|
|Tenure and Promotion Candidate|Individual|
|Mid-Probationary Candidate|Individual|
|Principal Lecturer|Individual|
|Senior Lecturer|Individual|
|Full Professor Candidate|Individual|
|Department Reviewers|Department|
|Department Chair|Department|
|Department Admin|Department|
|College Reviewers|College|
|College Dean|College|
|College Admin|College|
|Provost Faculty Reviewers|Provost|
|Senior Vice Provost|Provost|
|Provost|Provost|
|Provost Admin|Provost|
|Tech User|Tech|
