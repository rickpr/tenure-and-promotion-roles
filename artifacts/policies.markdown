|Subject|Object|Action|
|:--:|:--:|:--:|
|Tenure and Promotion Candidate|\*|none|
|Mid-Probationary Candidate|\*|none|
|Principal Lecturer|\*|none|
|Senior Lecturer|\*|none|
|Full Professor Candidate|\*|none|
|Department Reviewers|\*|none|
|Department Chair|\*|none|
|Department Admin|Tenure and Promotion Candidate|\*|
|Department Admin|Mid-Probationary Candidate|\*|
|Department Admin|Principal Lecturer|\*|
|Department Admin|Senior Lecturer|\*|
|College Reviewers|\*|none|
|College Dean|\*|none|
|College Admin|Department Admin|\*|
|College Admin|College Dean|\*|
|College Admin|College Reviewers|\*|
|Provost Faculty Reviewers|\*|none|
|Senior Vice Provost|\*|none|
|Provost|\*|none|
|Provost Admin|College Admin|\*|
|Provost Admin|Provost Faculty Reviewers|\*|
|Provost Admin|Senior Vice Provost|\*|
|Provost Admin|Provost|\*|
|Tech User|\*|\*|
