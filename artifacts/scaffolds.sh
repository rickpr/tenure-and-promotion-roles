rails g scaffold user netid
rails g rolify Role User
rails g scaffold master_role name context
rails g pundit:install
rails g pundit:policy user
rails g pundit:policy college
rails g pundit:policy department
rails g scaffold policy subject:references object:references action
