require 'test_helper'

class MasterRolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @master_role = master_roles(:one)
  end

  test "should get index" do
    get master_roles_url
    assert_response :success
  end

  test "should get new" do
    get new_master_role_url
    assert_response :success
  end

  test "should create master_role" do
    assert_difference('MasterRole.count') do
      post master_roles_url, params: { master_role: { context: @master_role.context, name: @master_role.name } }
    end

    assert_redirected_to master_role_url(MasterRole.last)
  end

  test "should show master_role" do
    get master_role_url(@master_role)
    assert_response :success
  end

  test "should get edit" do
    get edit_master_role_url(@master_role)
    assert_response :success
  end

  test "should update master_role" do
    patch master_role_url(@master_role), params: { master_role: { context: @master_role.context, name: @master_role.name } }
    assert_redirected_to master_role_url(@master_role)
  end

  test "should destroy master_role" do
    assert_difference('MasterRole.count', -1) do
      delete master_role_url(@master_role)
    end

    assert_redirected_to master_roles_url
  end
end
